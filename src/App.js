import React, {Component} from 'react';
import './App.css';

class App extends Component{

  constructor(props){
    super(props);
    this.state = {
      titulo: "Login",
      email: 'eduardo.lino@pucpr.br',
      senha: '123456',
      inputMail: '',
      inputSenha: '',
      mensagem: ''
    }

    this.validarAcesso = this.validarAcesso.bind(this);
  }

  mudaUser(event){
    let state = this.state;
    state.inputMail = event.target.value;
    this.setState(state);
    console.log(state.inputMail)
  }

  mudaSenha(event){
    let state = this.state;
    state.inputSenha = event.target.value;
    this.setState(state);
    console.log(state.inputSenha)
  }

  validarAcesso(){

    let state = this.state

    if(state.email === state.inputMail || state.senha === state.inputSenha){
      state.mensagem = 'Acessado com sucesso!'
    } else {
      state.mensagem = 'Usuário ou senha incorretos!'
    }
    this.setState(state);
    
  }

  render(){
    return (
      <header className='App-header'>
        <h2>{this.state.titulo}</h2>
        <div class='App-header-content'>
          <input type='text' id='user' class='loginField' placeholder='Usuário' name='usuario' onChange={(e) => this.mudaUser(e)}/>
          <input type='text' id='senha' class='loginField' placeholder='Senha' name='senha' onChange={(e) => this.mudaSenha(e)}/>
          <button onClick={this.validarAcesso} id='acesso'>Acessar</button>
          <label for='acesso' class='mensagemAcesso'>{this.state.mensagem}</label>
        </div>
      </header>
    )
  }

}

export default App;
